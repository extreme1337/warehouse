/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.dao;

import com.mysql.jdbc.PreparedStatement;
import com.singidunum.projectKDP.data.Suppliers;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Marko
 */
public class SuppliersDao {
    //Kreiranje Singleton paterna
    private static final SuppliersDao instance = new SuppliersDao();

    public SuppliersDao() {
    }

    public static SuppliersDao getInstance() {
        return instance;
    }
        //Kreiranje metoda za insert. Kao parametre sadrzi objekat na sebe i konekciju
    public int insert(Suppliers supplier, Connection con) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        int id = -1;
        try {
            //SQL komanda za inserovanje podatak u tabelu suppliers
            ps = (PreparedStatement) con.prepareStatement("INSERT INTO suppliers"
                    + "(SupplierName, ContactPerson, Address, City, PostCode, Country,"
                    + " Phone) VALUES(?,?,?) WHERE SupplierID=?",
                    Statement.RETURN_GENERATED_KEYS);
            //Komande za upisivanje u tableu na osnovu zadatog SQL upita
            ps.setString(1, supplier.getSupplierName());
            ps.setString(2, supplier.getContactPerson());
            ps.setString(3, supplier.getAddress());
            ps.setString(4, supplier.getCity());
            ps.setString(5, supplier.getPostCode());
            ps.setString(6, supplier.getCountry());
            ps.setString(7, supplier.getPhone());
            ps.setInt(8, supplier.getSupplierID());
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            rs.next();
            id = rs.getInt(1);
        } finally {
            //Zatvaranje resursa
            ResourcesManager.closeResources(rs, ps);
        }
        return id;
    }
//Kreiranje metoda za azuriranje podataka
    public void update(Suppliers supplier, Connection con) throws SQLException {
        PreparedStatement ps = null;
        try {
            //SQL komanda za azuriranje Suppliera sa odredjenim ID-om
            ps = (PreparedStatement) con.prepareStatement("UPDATE suppliers SET "
                    + "SupplierName=?, ContactPerson=?, Address=?,"
                    + "City=?, PostCode=?, Country=?, Phone=? WHERE SupplierID=?");
            //Komande koje izvrsavaju azuriranje
            ps.setString(1, supplier.getSupplierName());
            ps.setString(2, supplier.getContactPerson());
            ps.setString(3, supplier.getAddress());
            ps.setString(4, supplier.getCity());
            ps.setString(5, supplier.getPostCode());
            ps.setString(6, supplier.getCountry());
            ps.setString(7, supplier.getPhone());
            ps.setInt(8, supplier.getSupplierID());
            ps.executeUpdate();
        } finally {
            //Zatvaranje resursa
            ResourcesManager.closeResources(null, ps);
        }
    }
//Kreiranje metoda za brisanje iz Suppliers
    public void delete(int idSupplier, Connection con) throws SQLException {
        PreparedStatement ps = null;
        try {
            //SQL komanda za brisanje supplier-a iz tabele sa odredjenim ID-om
            ps = (PreparedStatement) con.prepareStatement("DELETE FROM suppliers WHERE SupplierID=?");
            ps.setInt(1, idSupplier);
            ps.executeUpdate();
        } finally {
            ResourcesManager.closeResources(null, ps);
        }
    }
//Kreiranje metoda za pronalazenje Supplier-a koja kao parametre ima konekciju i idSupplier
    public Suppliers find(int idSupplier, Connection con) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Suppliers supplier = null;
        try {
            //SQL upit za selektovanje Suppliera sa ordedjenim ID-om
            ps = (PreparedStatement) con.prepareStatement("SELECT * FROM suppliers WHERE SupplierID=?");
            ps.setInt(1, idSupplier);
            rs = ps.executeQuery();
            if (rs.next()) {
                supplier = new Suppliers(idSupplier, rs.getString("SupplierName"), 
                        rs.getString("ContactPerson"), rs.getString("Address"),
                        rs.getString("City"), rs.getString("PostCode"),
                        rs.getString("Country"),rs.getString("Phone"));
            }
        } finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return supplier;
    }
//Kreiranje metoda za izlistavanje svih Supplier-a
    public List<Suppliers> findAll(Connection con) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Suppliers> suppliersList = new ArrayList<>();
        try {
            ps = (PreparedStatement) con.prepareStatement("SELECT * FROM suppliers");
            rs = ps.executeQuery();
            while (rs.next()) {
                Suppliers suppliers = new Suppliers(rs.getInt("SupplierID"), 
                        rs.getString("SupplierName"), rs.getString("ContactPerson"),
                        rs.getString("Address"), rs.getString("City"),
                        rs.getString("PostCode"), rs.getString("Country"), rs.getString("Phone"));
                suppliersList.add(suppliers);
            }
        } finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return suppliersList;
    }
}
