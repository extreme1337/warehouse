/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.dao;

import com.mysql.jdbc.PreparedStatement;
import com.singidunum.projectKDP.data.Shippers;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Marko
 */
public class ShippersDao {
    private static final ShippersDao instance = new ShippersDao();

    private ShippersDao() {
    }

    public static ShippersDao getInstance() {
        return instance;
    }
    
        //Kreiranje metoda za insert. Kao parametre sadrzi objekat na sebe i konekciju
    public int insert(Shippers shipper, Connection con) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        int id = -1;
        try {
            //SQL komanda za inserovanje podatak u tabelu shippers
            ps = (PreparedStatement) con.prepareStatement("INSERT INTO shippers"
                    + "(ShipperName, Phone) VALUES(?,?)", Statement.RETURN_GENERATED_KEYS);
            //Komande za upisivanje u tableu na osnovu zadatog SQL upita
            ps.setString(1, shipper.getShipperName());
            ps.setString(2, shipper.getPhone());
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            rs.next();
            id = rs.getInt(1);
        } finally {
            //Zatvaranje resursa
            ResourcesManager.closeResources(rs, ps);
        }
        return id;
    }
//Kreiranje metoda za azuriranje podataka
    public void update(Shippers shipper, Connection con) throws SQLException {
        PreparedStatement ps = null;
        try {
            //SQL komanda za azuriranje Shipper sa odredjenim ID-om
            ps = (PreparedStatement) con.prepareStatement("UPDATE shippers SET "
                    + "ShipperName=?, Phone? WHERE ShipperID=?");
            //Komande koje izvrsavaju azuriranje
            ps.setString(1, shipper.getShipperName());
            ps.setString(2, shipper.getPhone());
            ps.setInt(3, shipper.getShipperID());
            ps.executeUpdate();
        } finally {
            //Zatvaranje resursa
            ResourcesManager.closeResources(null, ps);
        }
    }
//Kreiranje metoda za brisanje iz Shippers
    public void delete(int idShipper, Connection con) throws SQLException {
        PreparedStatement ps = null;
        try {
            //SQL komanda za brisanje shipper-a iz tabele sa odredjenim ID-om
            ps = (PreparedStatement) con.prepareStatement("DELETE FROM shippers WHERE ShipperID=?");
            ps.setInt(1, idShipper);
            ps.executeUpdate();
        } finally {
            ResourcesManager.closeResources(null, ps);
        }
    }
//Kreiranje metoda za pronalazenje Shipper-a koja kao parametre ima konekciju i idEmployee
    public Shippers find(int idShipper, Connection con) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Shippers shipper = null;
        try {
            //SQL upit za selektovanje Shipper-a sa ordedjenim ID-om
            ps = (PreparedStatement) con.prepareStatement("SELECT * FROM shippers WHERE ShipperID=?");
            ps.setInt(1, idShipper);
            rs = ps.executeQuery();
            if (rs.next()) {
                shipper = new Shippers(idShipper, rs.getString("ShipperName"), 
                        rs.getString("Phone"));
            }
        } finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return shipper;
    }
//Kreiranje metoda za izlistavanje svih Shippera-a
    public List<Shippers> findAll(Connection con) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Shippers> shippersList = new ArrayList<>();
        try {
            ps = (PreparedStatement) con.prepareStatement("SELECT * FROM shippers");
            rs = ps.executeQuery();
            while (rs.next()) {
                Shippers shippers = new Shippers(rs.getInt("ShipperID"), rs.getString("ShipperName")
                        , rs.getString("Phone"));
                shippersList.add(shippers);
            }
        } finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return shippersList;
    }
}
