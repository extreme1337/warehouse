/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.dao;

import com.mysql.jdbc.PreparedStatement;
import com.singidunum.projectKDP.data.Customers;
import com.singidunum.projectKDP.data.Employees;
import com.singidunum.projectKDP.data.Orders;
import com.singidunum.projectKDP.data.Shippers;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Marko
 */
//Kreiranje klase OrdersDao
public class OrdersDao {
//Kreiranje Singleton paterna
    private static final OrdersDao instance = new OrdersDao();

    private OrdersDao() {
    }

    public static OrdersDao getInstance() {
        return instance;
    }
//Kreiranje metoda za insertovanje Ordera
    public int insert(Orders order, Connection con) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        int id = -1;
        try {

            Integer fkCustomer = null;
            if (order.getFk_customer() != null) {
                //insetovanje customera i dobijanje vrijednosti ID-a 
                fkCustomer = CustomerDao.getInstance().insert(order.getFk_customer(), con);
            }
            Integer fkEmployee = null;
            if (order.getFk_employee()!= null) {
                //insetovanje customera i dobijanje vrijednosti ID-a 
                fkEmployee = EmployeesDao.getInstance().insert(order.getFk_employee(), con);
            }
            Integer fkShipper = null;
            if(order.getFk_shipper()!=null){
                //insetovanje customera i dobijanje vrijednosti ID-a 
                fkShipper = ShippersDao.getInstance().insert(order.getFk_shipper(), con);
            }
            //SQL upit za insetovanje novih ordera
            ps = (PreparedStatement) con.prepareStatement("INSERT INTO orders"
                    + "(OrderDate, FK_Customer,FK_employee, FK_Shipper) VALUES(?,?,?,?)",
                    Statement.RETURN_GENERATED_KEYS);
            ps.setDate(1, (Date) order.getOrderDate());
            ps.setInt(2, fkCustomer);
            ps.setInt(3, fkEmployee);
            ps.setInt(4, fkShipper);
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            rs.next();
            id = rs.getInt(1);

        } finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return id;
    }
//Kreiranje metode za updatovanje Ordera
    public void update(Orders order, Connection con) throws SQLException {
        PreparedStatement ps = null;
        try {
            //SQL upit za updatovanje Ordera
            ps = (PreparedStatement) con.prepareStatement("UPDATE orders SET OrderDate WHERE OrderID=?");
            ps.setDate(1, (Date) order.getOrderDate());
            ps.executeUpdate();
            
            if (order.getFk_customer() != null) {
                CustomerDao.getInstance().update(order.getFk_customer(), con);
            }
            if (order.getFk_employee()!= null) {
                EmployeesDao.getInstance().update(order.getFk_employee(), con);
            }
            if (order.getFk_shipper()!= null) {
                ShippersDao.getInstance().update(order.getFk_shipper(), con);
            }

        } finally {
            ResourcesManager.closeResources(null, ps);
        }
    }
//Kreiranje metoda za brisanje Ordera
    public void delete(int idOrder, Connection con) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            Orders order = new Orders(idOrder, rs.getDate("OrderDate"), 
                    rs.getObject("FK_Customer", Customers.class), 
                    rs.getObject("FK_Employee", Employees.class), rs.getObject("FK_Shipper", Shippers.class));
            //delete purchases
            OrderDetailsDao.getInstance().delete(order.getOrderID(), con);

            //Brisanje ordera
            ps = (PreparedStatement) con.prepareStatement("DELETE FROM orders WHERE OrderID=?");
            ps.setInt(1, idOrder);
            ps.executeUpdate();

            //Brisanje customera
            if (order.getFk_customer() != null) {
                CustomerDao.getInstance().delete(order.getFk_customer().getCustomerID(), con);
            }
            //Brisanje Employee
            if (order.getFk_employee()!= null) {
                EmployeesDao.getInstance().delete(order.getFk_employee().getEmployeeID(), con);
            }
            //Brisanje Shippera
            if (order.getFk_shipper()!= null) {
                ShippersDao.getInstance().delete(order.getFk_shipper().getShipperID(), con);
            }

        } finally {
            ResourcesManager.closeResources(null, ps);
        }
    }
//Kreiranje metoda za izlistavanje ordera sa ID-om
    public Orders find(int idOrder, Connection con) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Orders order = null;
        try {
            ps = (PreparedStatement) con.prepareStatement("SELECT * FROM orders WHERE OrderID=?");
            ps.setInt(1, idOrder);
            rs = ps.executeQuery();
            if (rs.next()) {
                Customers customer = CustomerDao.getInstance().find(rs.getInt("FK_Customer"), con);
                Employees employee = EmployeesDao.getInstance().find(rs.getInt("FK_Employee"), con);
                Shippers shipper = ShippersDao.getInstance().find(rs.getInt("FK_Shipper"), con);
                order = new Orders(rs.getInt("OrderID"),rs.getDate("OrderDate"), customer, employee, shipper);
            }
        } finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return order;
    }
    
    public List<Orders> findAll(Connection con) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Orders> ordersList = new ArrayList<>();
        try {
            ps = (PreparedStatement) con.prepareStatement("SELECT * FROM orders");
            rs = ps.executeQuery();
            while (rs.next()) {
                Customers customer = CustomerDao.getInstance().find(rs.getInt("FK_Customer"), con);
                Employees employee = EmployeesDao.getInstance().find(rs.getInt("FK_Employee"), con);
                Shippers shipper = ShippersDao.getInstance().find(rs.getInt("FK_Shipper"), con);
                Orders orders = new Orders(rs.getInt("OrderID"), rs.getDate("OrderDate"),
                        customer,employee,shipper);
                ordersList.add(orders);
            }
        } finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return ordersList;
    }
    
}
