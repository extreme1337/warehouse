/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.dao;

import com.mysql.jdbc.PreparedStatement;
import com.singidunum.projectKDP.data.Products;
import com.singidunum.projectKDP.data.Suppliers;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Marko
 */
public class ProductsDao {
    private static final ProductsDao instance = new ProductsDao();

    private ProductsDao() {
    }

    public static ProductsDao getInstance() {
        return instance;
    }

    public int insert(Products product, Connection con) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        int id = -1;
        try {
            Integer fkSupplier = null;
            if (product.getFk_supplier()!= null) {
                //insetovanje ordera i dobijanje vrijednosti ID-a 
                fkSupplier = SuppliersDao.getInstance().insert(product.getFk_supplier(), con);
            }
            //SQL komanda za inserovanje podatak u tabelu products
            ps = (PreparedStatement) con.prepareStatement("INSERT INTO products"
                    + "(ProductName, FK_Supplier, ProductCategory, PricePerUnit) "
                    + "VALUES(?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            //Komande za upisivanje u tableu na osnovu zadatog SQL upita
            ps.setString(1, product.getProductName());
            ps.setInt(2, fkSupplier);
            ps.setString(3, product.getProductCategory());
            ps.setFloat(4, product.getPricePerUnit());
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            rs.next();
            id = rs.getInt(1);
        } finally {
            //Zatvaranje resursa
            ResourcesManager.closeResources(rs, ps);
        }
        return id;
    }

    public void update(Products product, Connection con) throws SQLException {
        PreparedStatement ps = null;
        try {
            //SQL upit za updatovanje Ordera
            ps = (PreparedStatement) con.prepareStatement("UPDATE products SET ProductName=? WHERE ProductID=?");
            ps.setString(1, product.getProductName());
            ps.executeUpdate();
            
            if (product.getFk_supplier()!= null) {
                SuppliersDao.getInstance().update(product.getFk_supplier(), con);
            }

        } finally {
            ResourcesManager.closeResources(null, ps);
        }
    }

    public void delete(int idProduct, Connection con) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            Products product = new Products(idProduct, rs.getString("ProductName"), 
                    rs.getString("ProductCategory"), rs.getFloat("PricePerUnit"),
                    rs.getObject("FK_Supplier", Suppliers.class));
            //Brisanje OrderDetails
            OrderDetailsDao.getInstance().delete(product.getProductID(), con);

            //SQL upit za brisanje producta
            ps = (PreparedStatement) con.prepareStatement("DELETE FROM products WHERE ProductID=?");
            ps.setInt(1, idProduct);
            ps.executeUpdate();

            //Brisanje suppliera
            if (product.getFk_supplier()!= null) {
                SuppliersDao.getInstance().delete(product.getFk_supplier().getSupplierID(), con);
            }

        } finally {
            ResourcesManager.closeResources(rs, ps);
        }
    }

    public Products find(int idProduct, Connection con) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Products product = null;
        try {
            ps = (PreparedStatement) con.prepareStatement("SELECT * FROM products WHERE ProductID=?");
            ps.setInt(1, idProduct);
            rs = ps.executeQuery();
            if (rs.next()) {
                Suppliers supplier = SuppliersDao.getInstance().find(rs.getInt("FK_Supplier"), con);
                product = new Products(idProduct, rs.getString("ProductName"), 
                        rs.getString("ProductCategory"), rs.getFloat("PricePerUnit"), supplier);
            }
        } finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return product;
    }
    
    public List<Products> findAll(Connection con) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Products> productsList = new ArrayList<>();
        try {
            ps = (PreparedStatement) con.prepareStatement("SELECT * FROM orders");
            rs = ps.executeQuery();
            while (rs.next()) {
                Suppliers supplier = SuppliersDao.getInstance().find(rs.getInt("FK_Supplier"), con);
                Products product = new Products(rs.getInt("ProductID"), rs.getString("ProductName"), 
                        rs.getString("ProductCategory"), rs.getFloat("PricePerUnit"), supplier);
                productsList.add(product);
            }
        } finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return productsList;
    }
}
