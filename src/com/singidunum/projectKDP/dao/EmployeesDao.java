/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.dao;

import com.mysql.jdbc.PreparedStatement;
import com.singidunum.projectKDP.data.Employees;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Marko
 */
public class EmployeesDao {
    private static final EmployeesDao instance = new EmployeesDao();

    private EmployeesDao() {
    }

    public static EmployeesDao getInstance() {
        return instance;
    }
    
    //Kreiranje metoda za insert. Kao parametre sadrzi objekat na sebe i konekciju
    public int insert(Employees employee, Connection con) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        int id = -1;
        try {
            //SQL komanda za inserovanje podatak u tabelu employees
            ps = (PreparedStatement) con.prepareStatement("INSERT INTO employees"
                    + "(LastName, FirstName, BirthDate) VALUES(?,?,?)", Statement.RETURN_GENERATED_KEYS);
            //Komande za upisivanje u tableu na osnovu zadatog SQL upita
            ps.setString(1, employee.getLastName());
            ps.setString(2, employee.getFirstName());
            ps.setDate(3, (Date) employee.getBirthDate());
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            rs.next();
            id = rs.getInt(1);
        } finally {
            //Zatvaranje resursa
            ResourcesManager.closeResources(rs, ps);
        }
        return id;
    }
//Kreiranje metoda za azuriranje podataka
    public void update(Employees employee, Connection con) throws SQLException {
        PreparedStatement ps = null;
        try {
            //SQL komanda za azuriranje Employee sa odredjenim ID-om
            ps = (PreparedStatement) con.prepareStatement("UPDATE employees SET "
                    + "LastName=?, FirstName=?, BirthDate=? WHERE EmployeeID=?");
            //Komande koje izvrsavaju azuriranje
            ps.setString(1, employee.getLastName());
            ps.setString(2, employee.getFirstName());
            ps.setDate(3, (Date) employee.getBirthDate());
            ps.setInt(4, employee.getEmployeeID());
            ps.executeUpdate();
        } finally {
            //Zatvaranje resursa
            ResourcesManager.closeResources(null, ps);
        }
    }
//Kreiranje metoda za brisanje iz Employees
    public void delete(int idEmployee, Connection con) throws SQLException {
        PreparedStatement ps = null;
        try {
            //SQL komanda za brisanje employee-a iz tabele sa odredjenim ID-om
            ps = (PreparedStatement) con.prepareStatement("DELETE FROM employees WHERE EmployeeID=?");
            ps.setInt(1, idEmployee);
            ps.executeUpdate();
        } finally {
            ResourcesManager.closeResources(null, ps);
        }
    }
//Kreiranje metoda za pronalazenje Employee-a koja kao parametre ima konekciju i idEmployee
    public Employees find(int idEmployee, Connection con) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Employees employee = null;
        try {
            //SQL upit za selektovanje Employee-a sa ordedjenim ID-om
            ps = (PreparedStatement) con.prepareStatement("SELECT * FROM emlpoyees WHERE EmployeeID=?");
            ps.setInt(1, idEmployee);
            rs = ps.executeQuery();
            if (rs.next()) {
                employee = new Employees(idEmployee, rs.getString("LastName"), 
                        rs.getString("FirstName"), rs.getDate("BirthDate"));
            }
        } finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return employee;
    }
//Kreiranje metoda za izlistavanje svih Employee-a
    public List<Employees> findAll(Connection con) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Employees> employeesList = new ArrayList<>();
        try {
            ps = (PreparedStatement) con.prepareStatement("SELECT * FROM employees");
            rs = ps.executeQuery();
            while (rs.next()) {
                Employees employees = new Employees(rs.getInt("EmployeeID"), 
                        rs.getString("LastName"), rs.getString("FirstName"), rs.getDate("BirthDate"));
                employeesList.add(employees);
            }
        } finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return employeesList;
    }
}
