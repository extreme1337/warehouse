/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.dao;

import com.mysql.jdbc.PreparedStatement;
import com.singidunum.projectKDP.data.Customers;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Marko
 */
//Kreiranje DAO klase za Customera
public class CustomerDao {
    //Kreiranje Singleton paterna
    private static final CustomerDao instance = new CustomerDao();

    private CustomerDao() {
    }

    public static CustomerDao getInstance() {
        return instance;
    }
//Kreiranje metoda za insert. Kao parametre sadrzi objekat na sebe i konekciju
    public int insert(Customers customer, Connection con) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        int id = -1;
        try {
            //SQL komanda za inserovanje podatak u tabelu customer
            ps = (PreparedStatement) con.prepareStatement("INSERT INTO customers"
                    + "(CustomerName, ContactPerson, Address, City, PostCode, Country) "
                    + "VALUES(?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            //Komande za upisivanje u tableu na osnovu zadatog SQL upita
            ps.setString(1, customer.getCustomerName());
            ps.setString(2, customer.getContactPerson());
            ps.setString(3, customer.getAddress());
            ps.setString(4, customer.getCity());
            ps.setString(5, customer.getPostCode());
            ps.setString(6, customer.getCountry());
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            rs.next();
            id = rs.getInt(1);
        } finally {
            //Zatvaranje resursa
            ResourcesManager.closeResources(rs, ps);
        }
        return id;
    }
//Kreiranje metoda za azuriranje podataka
    public void update(Customers customer, Connection con) throws SQLException {
        PreparedStatement ps = null;
        try {
            //SQL komanda za azuriranje korisnika sa odredjenim ID-om
            ps = (PreparedStatement) con.prepareStatement("UPDATE customers SET CustomerName=?, "
                    + "ContactPerson=?, Address=?, City=? PostCode=?,"
                    + "Country=? WHERE CustomerID=?");
            //Komande koje izvrsavaju azuriranje
            ps.setString(1, customer.getCustomerName());
            ps.setString(2, customer.getContactPerson());
            ps.setString(3, customer.getAddress());
            ps.setString(4, customer.getCity());
            ps.setString(5, customer.getPostCode());
            ps.setString(6, customer.getCountry());
            ps.setInt(7, customer.getCustomerID());
            ps.executeUpdate();
        } finally {
            //Zatvaranje resursa
            ResourcesManager.closeResources(null, ps);
        }
    }
//Kreiranje metoda za brisanje iz Customera
    public void delete(int idCustomer, Connection con) throws SQLException {
        PreparedStatement ps = null;
        try {
            //SQL komanda za brisanje customera iz tabele sa odredjenim ID-om
            ps = (PreparedStatement) con.prepareStatement("DELETE FROM customers WHERE CustomerID=?");
            ps.setInt(1, idCustomer);
            ps.executeUpdate();
        } finally {
            ResourcesManager.closeResources(null, ps);
        }
    }
//Kreiranje metoda za pronalazenje Customera koja kao parametre ima konekciju i idCustomera
    public Customers find(int idCustomer, Connection con) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Customers customer = null;
        try {
            //SQL upit za selektovanje Customera sa ordedjenim ID-om
            ps = (PreparedStatement) con.prepareStatement("SELECT * FROM customers WHERE CustomerID=?");
            ps.setInt(1, idCustomer);
            rs = ps.executeQuery();
            if (rs.next()) {
                customer = new Customers(idCustomer, rs.getString("CustomerName"), 
                        rs.getString("ContactPerson"), rs.getString("Address"),
                        rs.getString("City"),rs.getString("PostCode"),rs.getString("Country"));
            }
        } finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return customer;
    }
//Kreiranje metoda za izlistavanje svih Customera
    public List<Customers> findAll(Connection con) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Customers> customersList = new ArrayList<>();
        try {
            ps = (PreparedStatement) con.prepareStatement("SELECT * FROM customers");
            rs = ps.executeQuery();
            while (rs.next()) {
                Customers customers = new Customers(rs.getInt("CustomerID"), 
                        rs.getString("CustomerName"), rs.getString("ContactPerson"), 
                        rs.getString("Address"), rs.getString("City"), 
                        rs.getString("PostCode"), rs.getString("Country"));
                customersList.add(customers);
            }
        } finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return customersList;
    }
}
