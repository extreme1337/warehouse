/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.dao;

import com.mysql.jdbc.PreparedStatement;
import com.singidunum.projectKDP.data.OrderDetails;
import com.singidunum.projectKDP.data.Orders;
import com.singidunum.projectKDP.data.Products;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Marko
 */
public class OrderDetailsDao {
    private static final OrderDetailsDao instance = new OrderDetailsDao();

    private OrderDetailsDao() {
    }

    public static OrderDetailsDao getInstance() {
        return instance;
    }
    
    public int insert(OrderDetails orderDetails, Connection con) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        int id = -1;
        try {

            Integer fkOrder = null;
            if (orderDetails.getFk_order() != null) {
                //insetovanje ordera i dobijanje vrijednosti ID-a 
                fkOrder = OrdersDao.getInstance().insert(orderDetails.getFk_order(), con);
            }
            Integer fkProduct = null;
            if (orderDetails.getFk_product()!= null) {
                //insetovanje producta i dobijanje vrijednosti ID-a 
                fkProduct = ProductsDao.getInstance().insert(orderDetails.getFk_product(), con);
            }
            //username, name, surname, credit, fk_contact_details, fk_address
            ps = (PreparedStatement) con.prepareStatement("INSERT INTO "
                    + "orderdetails(FK_Order, FK_Product, Quantity) VALUES(?,?,?)", 
                    Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, fkOrder);
            ps.setInt(2, fkProduct);
            ps.setFloat(3, orderDetails.getQuantity());
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            rs.next();
            id = rs.getInt(1);
        } finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return id;
    }
    
    public void update(OrderDetails orderDetails, Connection con) throws SQLException {
        PreparedStatement ps = null;
        try {

            ps = (PreparedStatement) con.prepareStatement("UPDATE orderdetails SET Quantity=? WHERE OrderDetailsID=?");
            ps.setInt(1, orderDetails.getQuantity());
            ps.executeUpdate();

            if (orderDetails.getFk_order()!= null) {
                OrdersDao.getInstance().update(orderDetails.getFk_order(), con);
            }
            if (orderDetails.getFk_product()!= null) {
                ProductsDao.getInstance().update(orderDetails.getFk_product(), con);
            }

        } finally {
            ResourcesManager.closeResources(null, ps);
        }
    }
    
    public void delete(int idOrderDetails, Connection con) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {

            OrderDetails orderDetails;
            orderDetails = new OrderDetails(idOrderDetails,rs.getInt("Quantity"),
                    rs.getObject("FK_Order",Orders.class), rs.getObject("FK_Product",Products.class));

            //SLQ upit za brisanje OrderDetails
            ps = (PreparedStatement) con.prepareStatement("DELETE FROM orderdetails WHERE OrderDetailsID=?");
            ps.setInt(1, idOrderDetails);
            ps.executeUpdate();

            //Brisanje Ordera
            if (orderDetails.getFk_order()!= null) {
                OrdersDao.getInstance().delete(orderDetails.getFk_order().getOrderID(), con);
            }

            //Brisanje Product-a
            if (orderDetails.getFk_product()!= null) {
                ProductsDao.getInstance().delete(orderDetails.getFk_product().getProductID(), con);
            }

        } finally {
            ResourcesManager.closeResources(rs, ps);
        }
    }
    
    public OrderDetails find(int idOrderDetails, Connection con) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        OrderDetails orderDetails = null;
        try {
            ps = (PreparedStatement) con.prepareStatement("SELECT * FROM orderdetails WHERE OrderDetailsID=?");
            ps.setInt(1, idOrderDetails);
            rs = ps.executeQuery();
            if (rs.next()) {
                Products product = ProductsDao.getInstance().find(rs.getInt("FK_Product"), con);
                Orders order = OrdersDao.getInstance().find(rs.getInt("FK_Order"), con);
                orderDetails= new OrderDetails(idOrderDetails, rs.getInt("Quantity"), order, product);
            }
        } finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return orderDetails;
    }
    
    public List<OrderDetails> findAll(Connection con) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<OrderDetails> orderDetailsList = new ArrayList<>();
        try {
            ps = (PreparedStatement) con.prepareStatement("SELECT * FROM orderdetails");
            rs = ps.executeQuery();
            while (rs.next()) {
                Orders order = OrdersDao.getInstance().find(rs.getInt("FK_Order"), con);
                Products product = ProductsDao.getInstance().find(rs.getInt("FK_Product"), con);
                OrderDetails orderDetails = new OrderDetails(rs.getInt("OrderDetailsID"), rs.getInt("Quantity"),
                        order,product);
                orderDetailsList.add(orderDetails);
            }
        } finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return orderDetailsList;
    }
}
