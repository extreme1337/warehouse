/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.dao;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.singidunum.projectKDP.exception.WarehouseException;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Marko
 */
//Kreiranje klase ResourcesManager u paketu dao za upravljanje sa resursima
public class ResourcesManager {

    static {
        //Ukljucivanje drivera u projekat
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
//Metod za dohvatanje konekcije
    public static Connection getConnection() throws SQLException {
        //Konektovanje na localhost na bazu warehouse sa user:root i password:
        Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/warehouse?user=root&password=");
        return con;
    }
//Metod za zatvaranje svih resursa
    public static void closeResources(ResultSet resultSet, PreparedStatement preparedStatement) throws SQLException {
        //Provjeravanje da li je result razlicit od null ako nije zatvara result
        if (resultSet != null) {
            resultSet.close();
        }
        //Provjeravanje da li je prepareStatment razlicit od null ako nije zatvara ga
        if (preparedStatement != null) {
            preparedStatement.close();
        }
    }
//Metod za zatvaranje konekcije
    public static void closeConnection(Connection con) throws WarehouseException {
        //Provjeravanje da li je konekcija razlicita od null
        if (con != null) {
            //Pokusavanje da se zatvori konekcija
            try {
                con.close();
            } catch (SQLException ex) {
                //Ako ne uspije da zatvori konekciju baca se izuzetak da je fejlovalo
                throw new WarehouseException("Failed to close database connection.", ex);
            }
        }
    }
//Kreiranje metoda za rollback
    public static void rollbackTransactions(Connection con) throws WarehouseException {
        //Provjerava da li ima neka konekcija
        if (con != null) {
            //Kada pronadje konekciju pokusava da uradi roolback
            try {
                con.rollback();
            } catch (SQLException ex) {
                //Ako ne uspije da izvrsi rollback baca se izuzetak da je fejlovao sa bacanjem izuzetka
                throw new WarehouseException("Failed to rollback database transactions.", ex);
            }
        }
    }
}