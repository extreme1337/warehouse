/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.exception;

/**
 *
 * @author Marko
 */
//Kreiranje klase za bacanje izuzetaka u paketu exception
public class WarehouseException extends Exception {
    
    public WarehouseException(String poruka) {
        super(poruka);
    }

    public WarehouseException(String message, Throwable cause) {
        super(message, cause);
    }
}
