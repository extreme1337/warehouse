/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.data;

import java.util.Date;

/**
 *
 * @author Marko
 */
//Kreiranje data klase za Employees
public class Employees {
    private int employeeID;
    private String lastName,firstName;
    private Date  birthDate;
//Kreiranje Employees konstruktora
    public Employees(int employeeID, String lastName, String firstName, Date birthDate) {
        this.employeeID = employeeID;
        this.lastName = lastName;
        this.firstName = firstName;
        this.birthDate = birthDate;
    }
//Kreiraje set-era i get-era
    public int getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(int employeeID) {
        this.employeeID = employeeID;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }
//toString metod
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Employee{").append("Last Name:").append(lastName)
                .append(", First Name").append(firstName)
                .append(", Birth Date:").append(birthDate).append("}");
        return sb.toString();
    }
    
    
}
