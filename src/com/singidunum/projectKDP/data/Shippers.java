/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.data;

/**
 *
 * @author Marko
 */
//Kreiranje data klase za Shippers
public class Shippers {
    private int shipperID;
    private String shipperName,phone;
//Kreiranje konstruktora

    public Shippers(int shipperID, String shipperName, String phone) {
        this.shipperID = shipperID;
        this.shipperName = shipperName;
        this.phone = phone;
    }
//Kreiranje set-era i get-era

    public int getShipperID() {
        return shipperID;
    }

    public void setShipperID(int shipperID) {
        this.shipperID = shipperID;
    }

    public String getShipperName() {
        return shipperName;
    }

    public void setShipperName(String shipperName) {
        this.shipperName = shipperName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
//toString metod

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Shippers{").append("Shipper Name:").append(shipperName)
                .append(", Phone:").append(phone);
        return sb.toString();
    }
    
}
