/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.data;

/**
 *
 * @author Marko
 */
//Kreiranje data klase OrderDetails sa spoljsnjim kljucevima na klase Orders i Products
public class OrderDetails {
    private int orderDetailsID,quantity;
    private Orders fk_order;
    private Products fk_product;

//Kreiranje konstruktora

    public OrderDetails(int orderDetailsID, int quantity, Orders fk_order, Products fk_product) {
        this.orderDetailsID = orderDetailsID;
        this.quantity = quantity;
        this.fk_order = fk_order;
        this.fk_product = fk_product;
    }


    
    
//Kreiranje set-era i get-era

    public int getOrderDetailsID() {
        return orderDetailsID;
    }

    public void setOrderDetailsID(int orderDetailsID) {
        this.orderDetailsID = orderDetailsID;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Orders getFk_order() {
        return fk_order;
    }

    public void setFk_order(Orders fk_order) {
        this.fk_order = fk_order;
    }

    public Products getFk_product() {
        return fk_product;
    }

    public void setFk_product(Products fk_product) {
        this.fk_product = fk_product;
    }
//toString metod

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Order Details{").append("Order Details ID:").append(orderDetailsID)
                .append(", Quantity:").append(quantity).append(", Order ID:")
                .append(fk_order).append(", Product ID:").append(fk_product).append("}");
        return sb.toString();
    }
    
}
