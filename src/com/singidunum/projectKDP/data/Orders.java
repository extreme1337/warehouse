/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.data;

import java.util.Date;

/**
 *
 * @author Marko
 */
//Kreiranje data klase Orders sa spoljsnjim kljucevima na klase Orders,Employees i Shippers
public class Orders {
    private int orderID;
    private Date orderDate;
    private Customers fk_customer;
    private Employees fk_employee;
    private Shippers fk_shipper;
    
//Kreiranje konstruktora

    public Orders(int orderID, Date orderDate, Customers fk_customer, Employees fk_employee, Shippers fk_shipper) {
        this.orderID = orderID;
        this.orderDate = orderDate;
        this.fk_customer = fk_customer;
        this.fk_employee = fk_employee;
        this.fk_shipper = fk_shipper;
    }

    
//Kreiranje set-era i get-era

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public Customers getFk_customer() {
        return fk_customer;
    }

    public void setFk_customer(Customers fk_customer) {
        this.fk_customer = fk_customer;
    }

    public Employees getFk_employee() {
        return fk_employee;
    }

    public void setFk_employee(Employees fk_employee) {
        this.fk_employee = fk_employee;
    }

    public Shippers getFk_shipper() {
        return fk_shipper;
    }

    public void setFk_shipper(Shippers fk_shipper) {
        this.fk_shipper = fk_shipper;
    }
    
//toString metod

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Orders{").append("Order ID:").append(orderID).append(", Order Date:")
                .append(orderDate).append(", Customer ID:").append(fk_customer)
                .append("Employee ID:").append(fk_employee).append(", Shipper ID:")
                .append(fk_shipper);
        return sb.toString();
    }
    
}
