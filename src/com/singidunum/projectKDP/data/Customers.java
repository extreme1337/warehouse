/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.data;

/**
 *
 * @author Marko
 */
//Kreiranje data klase za customera
public class Customers {
    private int customerID;
    private String customerName,contactPerson,address,city,postCode,country;
//Kreiranje konstruktora za Customera
    public Customers(int customerID, String customerName, String contactPerson, 
            String address, String city, String postCode, String country) {
        this.customerID = customerID;
        this.customerName = customerName;
        this.contactPerson = contactPerson;
        this.address = address;
        this.city = city;
        this.postCode = postCode;
        this.country = country;
    }
//Kreiranje set-era i get-era za Customera
    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
//toString metoda
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Customer{").append("Customer Name:").append(customerName)
                .append(", Contact Person:").append(contactPerson)
                .append(", Address:").append(address).append(", City:")
                .append(city).append(", Post Code:").append(postCode)
                .append(", Country:").append(country).append("}");
        return sb.toString();
    }
    
    
}
