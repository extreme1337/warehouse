/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.data;

/**
 *
 * @author Marko
 */
//Kreiranje data klase za Suppliers
public class Suppliers {
    private int supplierID;
    private String supplierName,contactPerson,address,city,postCode,country,phone;
//Kreiranje konstruktora za Suppliers
    public Suppliers(int supplierID, String supplierName, String contactPerson, String address, String city, String postCode, String country, String phone) {
        this.supplierID = supplierID;
        this.supplierName = supplierName;
        this.contactPerson = contactPerson;
        this.address = address;
        this.city = city;
        this.postCode = postCode;
        this.country = country;
        this.phone = phone;
    }
//Kreiranje set-era i get-era

    public int getSupplierID() {
        return supplierID;
    }

    public void setSupplierID(int supplierID) {
        this.supplierID = supplierID;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
//toString metod    

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Suppliers{").append("Supplier Name:").append(supplierName)
                .append(", Contact Person:").append(contactPerson)
                .append("Address:").append(address).append(", City:").append(city)
                .append(", Post Code:").append(postCode).append(", Country:").append(country)
                .append(", Phone:").append(phone).append("}");
        return sb.toString();
    }
    
    
}
