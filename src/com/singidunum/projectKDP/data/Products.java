/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.data;

/**
 *
 * @author Marko
 */
//Kreiranje klase Products sa spoljasnjim kljucem na klasu Suppliers
public class Products {
    private int productID;
    private String productName,productCategory;
    private float pricePerUnit;
    private Suppliers fk_supplier;
//Kreiranje konstruktora

    public Products(int productID, String productName, String productCategory, float pricePerUnit, Suppliers fk_supplier) {
        this.productID = productID;
        this.productName = productName;
        this.productCategory = productCategory;
        this.pricePerUnit = pricePerUnit;
        this.fk_supplier = fk_supplier;
    }
//Kreiranje set-era i get-era

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }

    public float getPricePerUnit() {
        return pricePerUnit;
    }

    public void setPricePerUnit(float pricePerUnit) {
        this.pricePerUnit = pricePerUnit;
    }

    public Suppliers getFk_supplier() {
        return fk_supplier;
    }

    public void setFk_supplier(Suppliers fk_supplier) {
        this.fk_supplier = fk_supplier;
    }
//toString metod

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Products{").append("Product Name:").append(productName)
                .append(", Product Category:").append(productCategory).append(", Price Per Unit:").append(pricePerUnit).append(", Supplier ID:").append(fk_supplier).append("}");
        return sb.toString();
    }
    
}
