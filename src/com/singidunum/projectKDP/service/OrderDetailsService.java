/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.service;

import com.singidunum.projectKDP.dao.OrderDetailsDao;
import com.singidunum.projectKDP.dao.ResourcesManager;
import com.singidunum.projectKDP.data.Customers;
import com.singidunum.projectKDP.data.Employees;
import com.singidunum.projectKDP.data.OrderDetails;
import com.singidunum.projectKDP.data.Orders;
import com.singidunum.projectKDP.data.Products;
import com.singidunum.projectKDP.data.Shippers;
import com.singidunum.projectKDP.data.Suppliers;
import com.singidunum.projectKDP.exception.WarehouseException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Marko
 */
public class OrderDetailsService {
    private static final OrderDetailsService instance = new OrderDetailsService();
    
    private OrderDetailsService() {}
    
    public static OrderDetailsService getInstance() {
        return instance;
    }
    
    public void makeOrder(OrderDetails orderDetails) throws WarehouseException, SQLException{
        Connection con=null;
        ResultSet rs = null;
        try{
            con=ResourcesManager.getConnection();
            con.setAutoCommit(false);
            Customers customer = new Customers(rs.getInt("CustomerID"), rs.getString("CustomerName"),
                    rs.getString("ContactPerson"), rs.getString("Address"), 
                    rs.getString("City"), rs.getString("PostCode"), rs.getString("Country"));
            if(orderDetails.getQuantity()==0){
                throw new WarehouseException("We don't have this item right now.");
            }
            if(!customer.getCountry().equals("Serbian")){
                throw new WarehouseException("We dont ship items out of Serbia");
            }
            Employees fk_employee = new Employees(rs.getInt("EmployeeID"),
                    rs.getString("LastName"),rs.getString("FirstName"), rs.getDate("BirthDate"));
            Shippers fk_shipper = new Shippers(rs.getInt("ShipperID"), rs.getString("ShipperName"),
                    rs.getString("Phone"));
            Suppliers fk_supplier = new Suppliers(rs.getInt("SupplierId"), rs.getString("SupplierName"),
                    rs.getString("ContactPerson"), rs.getString("Address"), 
                    rs.getString("City"), rs.getString("PostCode"), rs.getString("Country"),
                    rs.getString("Phone"));
            Orders fk_order = new Orders(rs.getInt("OrderID"), rs.getDate("OrderDate"),
                    customer, fk_employee, fk_shipper);
            Products fk_product = new Products(rs.getInt("ProductID"), rs.getString("ProductName"),
                    rs.getString("ProductCategory"), rs.getFloat("PricePerUnit"), fk_supplier);
            
            orderDetails = new OrderDetails(rs.getInt("OrderDetailsID"), rs.getInt("Quantity"), fk_order, fk_product);
            orderDetails.setQuantity(orderDetails.getQuantity()-1);
            OrderDetailsDao.getInstance().insert(orderDetails, con);
        }catch(Exception e){
            ResourcesManager.rollbackTransactions((com.mysql.jdbc.Connection) con);
            throw new WarehouseException("You can order this product.");
        }finally{
            ResourcesManager.closeConnection((com.mysql.jdbc.Connection) con);
            ResourcesManager.closeResources(rs, null);
        }
    }
}
