/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.service;

import com.singidunum.projectKDP.dao.ProductsDao;
import com.singidunum.projectKDP.dao.ResourcesManager;
import com.singidunum.projectKDP.dao.SuppliersDao;
import com.singidunum.projectKDP.data.Products;
import com.singidunum.projectKDP.data.Suppliers;
import com.singidunum.projectKDP.exception.WarehouseException;
import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author Marko
 */
public class ProductService {
    private static final ProductService instance = new ProductService();
    
    private ProductService() {}
    
    public static ProductService getInstance() {
        return instance;
    }
    
    public void addNewProduct(Products product) throws WarehouseException, SQLException {
        Connection conn = null;
        
        try {
            conn = ResourcesManager.getConnection();
            conn.setAutoCommit(false);
            ProductsDao.getInstance().insert(product, conn);
            conn.commit();
        } catch (SQLException e) {
            ResourcesManager.rollbackTransactions((com.mysql.jdbc.Connection) conn);
            throw new WarehouseException("Failed to add new Product");
        } finally {
            ResourcesManager.closeConnection((com.mysql.jdbc.Connection) conn);
        }
    }
    public void deleteProduct(int idProduct) throws WarehouseException {
        Connection conn = null;
        
        try {
            conn = ResourcesManager.getConnection();
            conn.setAutoCommit(false);
            Products product = ProductsDao.getInstance().find(idProduct, conn);            
            if(product != null) {
                ProductsDao.getInstance().delete(idProduct, conn);
            }
            conn.commit();
        } catch (Exception e) {
            ResourcesManager.rollbackTransactions((com.mysql.jdbc.Connection) conn);
            throw new WarehouseException("Failed to delete Product with given ID");
        } finally {
            ResourcesManager.closeConnection((com.mysql.jdbc.Connection) conn);
        }
    }
    public void updateProduct(Products product) throws WarehouseException {
        Connection conn = null;
        
        try {
            conn = ResourcesManager.getConnection();
            conn.setAutoCommit(false);
            ProductsDao.getInstance().update(product, conn);
            conn.commit();
        } catch (Exception e) {
            ResourcesManager.rollbackTransactions((com.mysql.jdbc.Connection) conn);
            throw new WarehouseException("Failed to update Product");
        } finally {
            ResourcesManager.closeConnection((com.mysql.jdbc.Connection) conn);
        }
    }
    
    public int addNewProductSupplier(Suppliers supplier) throws WarehouseException {
        Connection conn = null;
        
        try {
            conn = ResourcesManager.getConnection();
            return SuppliersDao.getInstance().insert(supplier, conn);
        } catch (SQLException e) {
            throw new WarehouseException("Failed to add new Product Supplier");
        } finally {
            ResourcesManager.closeConnection((com.mysql.jdbc.Connection) conn);
        }
    }
    public Suppliers findProductSupplier(int idSupplier) throws WarehouseException {
        Connection conn = null;
        
        try {
            conn = ResourcesManager.getConnection();
            return SuppliersDao.getInstance().find(idSupplier, conn);
        } catch (SQLException e) {
            throw new WarehouseException("Failed to find the Supplier with given ID");
        } finally {
            ResourcesManager.closeConnection((com.mysql.jdbc.Connection) conn);
        }
    }
    
    public void deleteProductSuplier(int idSupplier) throws WarehouseException{
        Connection conn = null;
        
        try{
            conn = ResourcesManager.getConnection();
            conn.setAutoCommit(false);
            Suppliers supplier = SuppliersDao.getInstance().find(idSupplier, conn);
            if(supplier!=null){
                SuppliersDao.getInstance().delete(idSupplier, conn);
            }
            conn.commit();
        }catch(SQLException e){
            throw new WarehouseException("Can't delete Product Suppleier for given ID");
        }finally{
            ResourcesManager.closeConnection((com.mysql.jdbc.Connection) conn);
        }
    }
}
