/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.service;

import com.mysql.jdbc.PreparedStatement;
import com.singidunum.projectKDP.dao.ResourcesManager;
import com.singidunum.projectKDP.exception.WarehouseException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Marko
 */
public class AdvancedService {
    private static final AdvancedService instance = new AdvancedService();
    
    private AdvancedService() {}
    
    public static AdvancedService getInstance() {
        return instance;
    }
 
    public void selectAllCustomersAndTheirOrders() throws WarehouseException, SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            con = ResourcesManager.getConnection();
            ps = (PreparedStatement) con.prepareStatement("SELECT customers.CustmerName , orders.OrderID "
                    + "FROM customers LEFT JOIN orders ON customers.CustomerID = orders.FK_Customer");
            ps.execute();
        }catch(SQLException e){
            throw new WarehouseException("Error");
        }finally{
            ResourcesManager.closeConnection((com.mysql.jdbc.Connection) con);
            ResourcesManager.closeResources(rs, ps);
        }
    }
    
    public void selectAllProductAndTheierSuppliers(int idSupplier) throws WarehouseException, SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            con = ResourcesManager.getConnection();
            ps = (PreparedStatement) con.prepareStatement("SELECT products.ProductName"
                    + "FROM products LEFT JOIN suppliers ON products.FK_Supplier= suppliers.SupplierID WHERE"
                    + "SupplierID=?");
            ps.setInt(1, idSupplier);
            ps.execute();
        }catch(SQLException e){
            throw new WarehouseException("Error");
        }finally{
            ResourcesManager.closeConnection((com.mysql.jdbc.Connection) con);
            ResourcesManager.closeResources(rs, ps);
        }
    }
    
    public void selectAllProductAndTheierShippers(int idShipper) throws WarehouseException, SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            con = ResourcesManager.getConnection();
            ps = (PreparedStatement) con.prepareStatement("SELECT products.ProductName FROM products "
                    + "LEFT OUTER JOIN suppliers ON products.FK_Supplier= suppliers.SupplierID "
                    + "LEFT OUTER JOIN orderdetails ON products.ProductID = orderdetails.FK_Product"
                    + "LEFT OUTER JOIN orders ON orderdetails.FK_Order = orders.OrderID"
                    + "LEFT OUTER JOIN shippers ON orders.FK_Shipper = shippers.ShipperID"
                    + "WHERE ShipperID=?");
            ps.setInt(1, idShipper);
        }catch(SQLException e){
            throw new WarehouseException("Error");
        }finally{
            ResourcesManager.closeConnection((com.mysql.jdbc.Connection) con);
            ResourcesManager.closeResources(rs, ps);
        }
    }
    
    public void sumOfAllPurchases() throws WarehouseException, SQLException{
        Connection conn = null;
        PreparedStatement ps = null;
        try{
            conn = ResourcesManager.getConnection();
            ps = (PreparedStatement) conn.prepareStatement("SELECT (SELECT Quantity FROM orderdetails) * (SELECT PricePerUnit FROM products) "
                    + "AS sumOfAllPurchases FROM orderdetails FULL OUTER JOIN products ON orderdetails.FK_Product = products.ProductID");
            ps.execute();
        }catch(SQLException e){
            throw new WarehouseException("Error");
        }finally{
            ResourcesManager.closeConnection((com.mysql.jdbc.Connection) conn);
            ResourcesManager.closeResources(null, ps);
        }
    }
    
    public void sumOfPurchasesCustomer(int idCustomer) throws WarehouseException, SQLException{
        Connection conn = null;
        PreparedStatement ps = null;
        try{
            conn = ResourcesManager.getConnection();
            ps = (PreparedStatement) conn.prepareStatement("SELECT (SELECT Quantity FROM orderdetails) * (SELECT PricePerUnit FROM products) "
                    + "AS sumOfAllPurchases FROM orderdetails FULL OUTER JOIN products ON orderdetails.FK_Product = products.ProductID"
                    + "FULL OUTER JOIN orders ON orderdetails.FK_Order = orders.OrderID"
                    + "FULL OUTER JOIN customers ON orders.FK_Customer = customers.CustomerID"
                    + "WHERE CustomerID=?");
            ps.setInt(1, idCustomer);
            ps.execute();
        }catch(SQLException e){
            throw new WarehouseException("Error");
        }finally{
            ResourcesManager.closeConnection((com.mysql.jdbc.Connection) conn);
            ResourcesManager.closeResources(null, ps);
        }
    }
    
    public void sumOfPurchasesShipper(int idShipper) throws WarehouseException, SQLException{
        Connection conn = null;
        PreparedStatement ps = null;
        try{
            conn = ResourcesManager.getConnection();
            ps = (PreparedStatement) conn.prepareStatement("SELECT (SELECT Quantity FROM orderdetails) * (SELECT PricePerUnit FROM products) "
                    + "AS sumOfAllPurchases FROM orderdetails FULL OUTER JOIN products ON orderdetails.FK_Product = products.ProductID"
                    + "FULL OUTER JOIN orders ON orderdetails.FK_Order = orders.OrderID"
                    + "FULL OUTER JOIN shippers ON orders.FK_Shipper = shippers.ShipperID"
                    + "WHERE ShipperID=?");
            ps.setInt(1, idShipper);
            ps.execute();
        }catch(SQLException e){
            throw new WarehouseException("Error");
        }finally{
            ResourcesManager.closeConnection((com.mysql.jdbc.Connection) conn);
            ResourcesManager.closeResources(null, ps);
        }
    }
    
    public void sumOfPurchasesSupplier(int idSupplier) throws WarehouseException, SQLException{
        Connection conn = null;
        PreparedStatement ps = null;
        try{
            conn = ResourcesManager.getConnection();
            ps = (PreparedStatement) conn.prepareStatement("SELECT (SELECT Quantity FROM orderdetails) * (SELECT PricePerUnit FROM products) "
                    + "AS sumOfAllPurchases FROM orderdetails FULL OUTER JOIN products ON orderdetails.FK_Product = products.ProductID"
                    + "FULL OUTER JOIN suppliers ON products.FK_Supplier = suppliers.SupplierID"
                    + "WHERE SupplierID=?");
            ps.setInt(1, idSupplier);
            ps.execute();
        }catch(SQLException e){
            throw new WarehouseException("Error");
        }finally{
            ResourcesManager.closeConnection((com.mysql.jdbc.Connection) conn);
            ResourcesManager.closeResources(null, ps);
        }
    }
    
    public void theBestEmployeeSale() throws SQLException, WarehouseException{
        Connection conn = null;
        PreparedStatement ps = null;
        try{
            conn = ResourcesManager.getConnection();
            ps = (PreparedStatement) conn.prepareStatement("SELECT employees.FirstName, employees.LastName,"
                    + "(SELECT Quantity FROM orderdetails) * (SELECT PricePerUnit FROM products) AS theBestPrices"
                    + "FROM orderdetails FULL OUTER JOIN products ON orderdetails.FK_Product = products.ProductID"
                    + "FULL OUTER JOIN orders ON orderdetails.FK_Order = orders.OrderID"
                    + "FULL OUTER JOIN employees ON orders.FK_Employee = employees.EmployeeID"
                    + "WHERE max(orderdetails.Quantity*products.PricePerUnit)");
        }catch(SQLException e){
            throw new WarehouseException("Error");
        }finally{
            ResourcesManager.closeConnection((com.mysql.jdbc.Connection) conn);
            ResourcesManager.closeResources(null, ps);
        }
    }
}
