/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.service;

import com.singidunum.projectKDP.dao.CustomerDao;
import com.singidunum.projectKDP.dao.EmployeesDao;
import com.singidunum.projectKDP.dao.OrdersDao;
import com.singidunum.projectKDP.dao.ResourcesManager;
import com.singidunum.projectKDP.dao.ShippersDao;
import com.singidunum.projectKDP.data.Customers;
import com.singidunum.projectKDP.data.Employees;
import com.singidunum.projectKDP.data.Orders;
import com.singidunum.projectKDP.data.Shippers;
import com.singidunum.projectKDP.exception.WarehouseException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Marko
 */
public class OrderService {
    private static final OrderService instance = new OrderService();
    
    private OrderService() {}
    
    public static OrderService getInstance() {
        return instance;
    }
    
    public void insertOrder(Customers customer,Employees employee,Shippers shipper) throws WarehouseException, SQLException{
        Connection conn = null;
        ResultSet rs = null;
        try{
            conn = ResourcesManager.getConnection();
            conn.setAutoCommit(false);
            
            customer = new Customers(rs.getInt("CustomerID"), rs.getString("CustomerName"),
                    rs.getString("ContactPerson"), rs.getString("Address"), rs.getString("City"),
                    rs.getString("PostCode"), rs.getString("Country"));
            employee = new Employees(rs.getInt("EmployeeID"), rs.getString("LastName"),
                    rs.getString("FirstName"), rs.getDate("BirthDate"));
            shipper = new Shippers(rs.getInt("ShipperID"), rs.getString("ShipperName"),
                    rs.getString("Phone"));
            Orders order = new Orders(rs.getInt("OrderID"), rs.getDate("OrderDate"),
                    customer, employee, shipper);
            OrdersDao.getInstance().insert(order, conn);
            conn.commit();
        }catch (Exception e){
            ResourcesManager.rollbackTransactions((com.mysql.jdbc.Connection) conn);
            throw new WarehouseException("Failed to make order.");
        }finally{
           ResourcesManager.closeConnection((com.mysql.jdbc.Connection) conn);
           ResourcesManager.closeResources(rs, null);
        }
    }
    public int addNewOrder(Orders order) throws WarehouseException {
        Connection conn = null;
        
        try {
            conn = ResourcesManager.getConnection();
            return OrdersDao.getInstance().insert(order, conn);
        } catch (SQLException e) {
            throw new WarehouseException("Failed to add new Order");
        } finally {
            ResourcesManager.closeConnection((com.mysql.jdbc.Connection) conn);
        }
    }
    public int addNewOrderCustomer(Customers customer) throws WarehouseException {
        Connection conn = null;
        
        try {
            conn = ResourcesManager.getConnection();
            return CustomerDao.getInstance().insert(customer, conn);
        } catch (SQLException e) {
            throw new WarehouseException("Failed to add new Order Customer");
        } finally {
            ResourcesManager.closeConnection((com.mysql.jdbc.Connection) conn);
        }
    }
    
    public int addNewOrderEmployee(Employees employee) throws WarehouseException {
        Connection conn = null;
        
        try {
            conn = ResourcesManager.getConnection();
            return EmployeesDao.getInstance().insert(employee, conn);
        } catch (SQLException e) {
            throw new WarehouseException("Failed to add new Order Employee");
        } finally {
            ResourcesManager.closeConnection((com.mysql.jdbc.Connection) conn);
        }
    }
    
    public int addNewOrderShipper(Shippers shipper) throws WarehouseException {
        Connection conn = null;
        
        try {
            conn = ResourcesManager.getConnection();
            return ShippersDao.getInstance().insert(shipper, conn);
        } catch (SQLException e) {
            throw new WarehouseException("Failed to add new Order Shipper");
        } finally {
            ResourcesManager.closeConnection((com.mysql.jdbc.Connection) conn);
        }
    }
    
    public Orders findOrder(int idOrder) throws WarehouseException {
        Connection conn = null;
        
        try {
            conn = ResourcesManager.getConnection();
            return OrdersDao.getInstance().find(idOrder, conn);
        } catch (SQLException e) {
            throw new WarehouseException("Failed to find the Product with given ID");
        } finally {
            ResourcesManager.closeConnection((com.mysql.jdbc.Connection) conn);
        }
    }
    
    public Customers findOrderCustomer(int idCustomer) throws WarehouseException {
        Connection conn = null;
        
        try {
            conn = ResourcesManager.getConnection();
            return CustomerDao.getInstance().find(idCustomer, conn);
        } catch (SQLException e) {
            throw new WarehouseException("Failed to find the Customer with given ID");
        } finally {
            ResourcesManager.closeConnection((com.mysql.jdbc.Connection) conn);
        }
    }
    
    public Employees findOrderEmployee(int idEmployee) throws WarehouseException {
        Connection conn = null;
        
        try {
            conn = ResourcesManager.getConnection();
            return EmployeesDao.getInstance().find(idEmployee, conn);
        } catch (SQLException e) {
            throw new WarehouseException("Failed to find the Employee with given ID");
        } finally {
            ResourcesManager.closeConnection((com.mysql.jdbc.Connection) conn);
        }
    }
    
    public Shippers findOrderShipper(int idShipper) throws WarehouseException {
        Connection conn = null;
        
        try {
            conn = ResourcesManager.getConnection();
            return ShippersDao.getInstance().find(idShipper, conn);
        } catch (SQLException e) {
            throw new WarehouseException("Failed to find the Shipper with given ID");
        } finally {
            ResourcesManager.closeConnection((com.mysql.jdbc.Connection) conn);
        }
    }
    
    public void updateOrder(Orders order) throws WarehouseException {
        Connection conn = null;
        
        try {
            conn = ResourcesManager.getConnection();
            conn.setAutoCommit(false);
            OrdersDao.getInstance().update(order, conn);
            conn.commit();
        } catch (SQLException e) {
            ResourcesManager.rollbackTransactions((com.mysql.jdbc.Connection) conn);
            throw new WarehouseException("Failed to delete given Order");
        } finally {
            ResourcesManager.closeConnection((com.mysql.jdbc.Connection) conn);
        }
    }
    
    public void deleteOrder(int idOrder) throws WarehouseException {
        Connection conn = null;
        
        try {
            conn = ResourcesManager.getConnection(); 
            conn.setAutoCommit(false);
            Orders order = OrdersDao.getInstance().find(idOrder, conn);
            if(order != null) {
                OrdersDao.getInstance().delete(idOrder, conn);
            }
            conn.commit();
        } catch (SQLException e) {
            ResourcesManager.rollbackTransactions((com.mysql.jdbc.Connection) conn);
            throw new WarehouseException("Faild to delete Order with given ID");
        } finally {
            ResourcesManager.closeConnection((com.mysql.jdbc.Connection) conn);
        }
    }
    
    
}
