/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warehouse1;

import com.singidunum.projectKDP.data.Customers;
import com.singidunum.projectKDP.data.Employees;
import com.singidunum.projectKDP.data.OrderDetails;
import com.singidunum.projectKDP.data.Orders;
import com.singidunum.projectKDP.data.Products;
import com.singidunum.projectKDP.data.Shippers;
import com.singidunum.projectKDP.data.Suppliers;
import com.singidunum.projectKDP.exception.WarehouseException;
import com.singidunum.projectKDP.service.AdvancedService;
import com.singidunum.projectKDP.service.OrderDetailsService;
import com.singidunum.projectKDP.service.OrderService;
import com.singidunum.projectKDP.service.ProductService;
import java.sql.SQLException;
import java.util.Date;

/**
 *
 * @author Marko
 */
public class Warehouse1 {

    /**
     * @param args the command line arguments
     */
    private static final OrderDetailsService orderDetailsService = OrderDetailsService.getInstance();
    private static final OrderService orderService = OrderService.getInstance();
    private static final ProductService productService = ProductService.getInstance();
    private static final AdvancedService advancedService = AdvancedService.getInstance();
    
    
    private static void addOrder() throws WarehouseException, SQLException {
        orderService.insertOrder(new Customers(1, "Pera", "pera@gmail.com", "Kumodraska 261", 
                "Beograd", "11000", "Serbia"), new Employees(1, "Zikic", "Zika", new Date(25-10-1988)), 
                new Shippers(1, "Lazar", "00381664448899"));
    }
    private static void addOrderDetails() throws WarehouseException, SQLException{
        orderDetailsService.makeOrder(new OrderDetails(1, 2, new Orders(1, new Date(25-6-2017), new Customers(1, "Pera", "pera@gmail.com", "Kumodraska 261", 
                "Beograd", "11000", "Serbia"), new Employees(1, "Zikic", "Zika", new Date(25-10-1988)), 
                new Shippers(1, "Lazar", "00381664448899")), new Products(1, "Lord of the Rings", "Book", 25, new Suppliers(1, "Svetozar", 
                        "svetozar@gmail.com", "Danijelova 32", "Beograd", "11000", "Srbija", "00381698789874"))));
    }
    
   private static void addProduct() throws WarehouseException, SQLException{
       productService.addNewProduct(new Products(1, "Mali pricn", "Book", (float) 12.3, new Suppliers(1, "Svetozar", 
                        "svetozar@gmail.com", "Danijelova 32", "Beograd", "11000", "Srbija", "00381698789874")));
   }
    public static void main(String[] args) throws WarehouseException, SQLException {
        // TODO code application logic here
        addProduct();
        addOrderDetails();
        addOrder();
    }
    
}
